#include <chrono>
#include <iostream>
#include <functional>
#include <map>
#include <memory>
#include <string>
#include <thread>

class Stopwatch;


class State {
private:
    friend class Stopwatch;

    std::function<void()> enter;
    std::function<void()> exit;

public:
    State() {
        this->enter = []() {};
        this->exit = []() {};
    }

    State(std::function<void()> enter, std::function<void()> exit) {
        this->enter = enter;
        this->exit = exit;
    }
};


class Stopwatch {
private:
    using clock = std::chrono::steady_clock;

    std::shared_ptr<State> current_state;
    std::shared_ptr<State> stop_state;
    std::shared_ptr<State> start_state;
    std::shared_ptr<State> reset_state;

    clock::time_point t_start, t_stop;
    bool running;

    void select_state(std::shared_ptr<State> new_state) {
        if (current_state != nullptr) {
            current_state->exit();
        }

        current_state = new_state;
        current_state->enter();
    }

public:
    Stopwatch() {
        stop_state = std::make_shared<State>(
            [this]() {
                this->t_stop = clock::now();
                this->running = false;
            },
            []() {}
        );

        start_state = std::make_shared<State>(
            [this]() {
                this->t_start = clock::now();
                this->running = true;
            },
            []() {}
        );

        reset_state = std::make_shared<State>(
            [this]() {
                this->t_start = clock::now();
                this->t_stop = t_start;
            },
            []() {}
        );
    }

    ~Stopwatch() {
        if (current_state != nullptr) {
            current_state->exit();
        }
    }

    void start() {
        select_state(start_state);
    }

    void stop() {
        select_state(stop_state);
    }

    void reset() {
        auto orig_state = current_state;

        // reset timer
        select_state(reset_state);

        // go back to the previous state
        select_state(orig_state);
    }

    auto get_elapsed_time() {
        if (!this->running) {
            return t_stop - t_start;
        }
        else {
            return clock::now() - t_start;
        }
    }

    auto get_elapsed_time_ms() -> int {
        return std::chrono::duration_cast<std::chrono::milliseconds>(get_elapsed_time()).count();
    }
};


int main() {
    using std::chrono::duration_cast;
    using std::chrono::milliseconds;

    Stopwatch stopwatch;

    stopwatch.start();
    std::this_thread::sleep_for(std::chrono::milliseconds(101));
    stopwatch.stop();

    auto elapsed = duration_cast<milliseconds>(stopwatch.get_elapsed_time()).count();
    std::cout << "elapsed time = " << elapsed << " ms\n";

    stopwatch.reset();

    elapsed = duration_cast<milliseconds>(stopwatch.get_elapsed_time()).count();
    std::cout << "after reset, elapsed time = " << elapsed << " ms\n";

    stopwatch.start();
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::cout << "elapsed time = " << stopwatch.get_elapsed_time_ms() << "ms\n";
    stopwatch.reset();
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    std::cout << "elapsed after reset while still running = "
              << stopwatch.get_elapsed_time_ms() << "ms\n";

    return 0;
}
